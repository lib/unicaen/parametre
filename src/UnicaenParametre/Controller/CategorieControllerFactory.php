<?php

namespace UnicaenParametre\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenParametre\Form\Categorie\CategorieForm;
use UnicaenParametre\Service\Categorie\CategorieService;
use UnicaenParametre\Service\Parametre\ParametreService;

class CategorieControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return CategorieController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : CategorieController
    {
        /**
         * @var CategorieService $categorieService
         * @var ParametreService $parametreService
         */
        $categorieService = $container->get(CategorieService::class);
        $parametreService = $container->get(ParametreService::class);

        /**
         * @var CategorieForm $categorieForm
         */
        $categorieForm = $container->get('FormElementManager')->get(CategorieForm::class);

        $controller = new CategorieController();
        $controller->setCategorieService($categorieService);
        $controller->setParametreService($parametreService);
        $controller->setCategorieForm($categorieForm);
        return $controller;
    }
}