<?php

namespace UnicaenParametre\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;

class ParametreValueViewHelper extends AbstractHelper
{
    use ParametreServiceAwareTrait;

    public function __invoke(string $categorieCode, string $parametreCode, array $options = []) : ?string
    {
        $value = $this->getParametreService()->getValeurForParametre($categorieCode, $parametreCode);
        if ($value === null) return null;
        return "".$value;
    }
}