<?php

namespace UnicaenParametre\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;

class ParametreValueViewHelperFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ParametreValueViewHelper
    {
        /** @var ParametreService $parametreService */
        $parametreService = $container->get(ParametreService::class);

        $helper = new ParametreValueViewHelper();
        $helper->setParametreService($parametreService);
        return $helper;
    }
}