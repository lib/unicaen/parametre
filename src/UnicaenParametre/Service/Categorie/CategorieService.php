<?php

namespace UnicaenParametre\Service\Categorie;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenParametre\Entity\Db\Categorie;

class CategorieService
{
    use ProvidesObjectManager;

    /** GESTION ENTITY ************************************************************************************************/

    public function create(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->persist($categorie);
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function update(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    public function delete(Categorie $categorie): Categorie
    {
        $this->getObjectManager()->remove($categorie);
        $this->getObjectManager()->flush($categorie);
        return $categorie;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Categorie::class)->createQueryBuilder('categorie');
        return $qb;
    }

    /** @return Categorie[] */
    public function getCategories(string $champ = "ordre", string $ordre = "ASC"): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('categorie.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return array
     */
    public function getCategoriesAsOptions(string $champ = "ordre", string $ordre = "ASC"): array
    {
        $categories = $this->getCategories($champ, $ordre);
        $array = [];
        foreach ($categories as $categorie) {
            $array[$categorie->getId()] = $categorie->getLibelle();
        }
        return $array;
    }

    public function getCategorie(?int $id): ?Categorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ParamatreCategorie partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getCategoriebyCode(string $code): ?Categorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.code = :code')
            ->setParameter('code', $code);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ParamatreCategorie partagent le même code [" . $code . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedCategorie(AbstractActionController $controller, string $param = 'categorie'): ?Categorie
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getCategorie($id);
        return $result;
    }
}