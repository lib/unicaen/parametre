<?php

namespace UnicaenParametre\Service\Parametre;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ParametreServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return ParametreService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ParametreService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new ParametreService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}