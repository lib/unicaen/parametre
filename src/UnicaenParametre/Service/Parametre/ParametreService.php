<?php

namespace UnicaenParametre\Service\Parametre;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use UnicaenParametre\Exception\ParametreMalTypeException;
use RuntimeException;
use UnicaenParametre\Entity\Db\Categorie;
use UnicaenParametre\Entity\Db\Parametre;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenParametre\Exception\ParametreNotFoundException;

class ParametreService
{
    use ProvidesObjectManager;

    /** GESTION ENTITY ************************************************************************************************/

    public function create(Parametre $parametre): Parametre
    {
        $this->getObjectManager()->persist($parametre);
        $this->getObjectManager()->flush($parametre);
        return $parametre;
    }

    public function update(Parametre $parametre): Parametre
    {
        $this->getObjectManager()->flush($parametre);
        return $parametre;
    }

    public function delete(Parametre $parametre): Parametre
    {
        $this->getObjectManager()->remove($parametre);
        $this->getObjectManager()->flush($parametre);
        return $parametre;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Parametre::class)->createQueryBuilder('parametre')
            ->addSelect('categorie')->join('parametre.categorie', 'categorie');
        return $qb;
    }

    /** @return Parametre[] */
    public function getParametres(string $champ = 'ordre', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('parametre.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Parametre[] */
    public function getParametresByCategorie(Categorie $categorie, string $champ = 'ordre', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('parametre.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->orderBy('parametre.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Parametre[] */
    public function getParametresByCategorieCode(string $categorieCode, string $champ = 'ordre', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.code = :categorie')
            ->setParameter('categorie', $categorieCode)
            ->orderBy('parametre.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();

        $dictionnaire = [];
        foreach ($result as $item) {
            $dictionnaire[$item->getCode()] = $item;
        }
        return $dictionnaire;
    }


    public function getParametre(int $id): ?Parametre
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('parametre.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Parametre partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getParametreByCode(string $categorieCode, string $parametreCode): ?Parametre
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('categorie.code = :categorieCode')
            ->andWhere('parametre.code = :parametreCode')
            ->setParameter('categorieCode', $categorieCode)
            ->setParameter('parametreCode', $parametreCode);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Parametre partagent le même code [" . $categorieCode . " - " . $parametreCode . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedParametre(AbstractActionController $controller, string $param = 'parametre') : ?Parametre
    {
        $id = $controller->params()->fromRoute($param);
        /** @var Parametre $parametre */
        $parametre = $this->getParametre($id);
        return $parametre;
    }

    /** FACADE ********************************************************************************************************/

    /**
     * @throws ParametreNotFoundException
     * @throws ParametreMalTypeException
     */
    public function getValeurForParametre(string $categorieCode, string $parametreCode): string|int|bool|null
    {
        $parametre = $this->getParametreByCode($categorieCode, $parametreCode);
        if ($parametre === null) {
            throw new ParametreNotFoundException("Aucun paramètre de trouvé pour les codes [Categorie: ".$categorieCode.",Parametre: ".$parametreCode."]");
        }

        if ($parametre->getValeur() === null) return null;

        if ($parametre->getValeursPossibles() === Parametre::TYPE_STRING)           return $parametre->getValeur();

        // Note : attention en bd c'est un string du coup si === alors 'true'
        if ($parametre->getValeursPossibles() === Parametre::TYPE_BOOLEAN)          return ($parametre->getValeur() === 'true');
        if ($parametre->getValeursPossibles() === Parametre::TYPE_NUMBER)           return ((int) $parametre->getValeur());

        throw new ParametreMalTypeException("Le type [".$parametre->getValeursPossibles()."] du paramètre [Categorie: ".$categorieCode.",Parametre: ".$parametreCode."] est non géré");
    }
}