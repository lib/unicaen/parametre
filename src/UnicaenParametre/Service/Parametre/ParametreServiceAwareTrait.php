<?php

namespace UnicaenParametre\Service\Parametre;


trait ParametreServiceAwareTrait
{
    protected ParametreService $parametreService;

    public function setParametreService(ParametreService $parametreService) : void
    {
        $this->parametreService = $parametreService;
    }

    public function getParametreService() : ParametreService
    {
        return $this->parametreService;
    }
}