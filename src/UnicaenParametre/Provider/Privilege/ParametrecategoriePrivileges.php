<?php

namespace UnicaenParametre\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ParametrecategoriePrivileges extends Privileges
{
    const PARAMETRECATEGORIE_INDEX = 'parametrecategorie-parametrecategorie_index';
    const PARAMETRECATEGORIE_AFFICHER = 'parametrecategorie-parametrecategorie_afficher';
    const PARAMETRECATEGORIE_AJOUTER = 'parametrecategorie-parametrecategorie_ajouter';
    const PARAMETRECATEGORIE_MODIFIER = 'parametrecategorie-parametrecategorie_modifier';
    const PARAMETRECATEGORIE_SUPPRIMER = 'parametrecategorie-parametrecategorie_supprimer';
}