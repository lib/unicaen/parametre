<?php

namespace UnicaenParametre\Form\Parametre;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenParametre\Service\Parametre\ParametreService;

class ParametreFormFactory {

    /**
     * @param ContainerInterface $container
     * @return ParametreForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ParametreForm
    {
        /**
         * @var ParametreService $parametreService
         * @var ParametreHydrator $hydrator
         */
        $parametreService = $container->get(ParametreService::class);
        $hydrator = $container->get('HydratorManager')->get(ParametreHydrator::class);

        $form = new ParametreForm();
        $form->setParametreService($parametreService);
        $form->setHydrator($hydrator);
        return $form;
    }
}