<?php

namespace UnicaenParametre\Form\Parametre;

use Laminas\Form\Element\Checkbox;
use UnicaenParametre\Entity\Db\Categorie;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;

class ParametreForm extends Form {
    use ParametreServiceAwareTrait;

    public function init(): void
    {
        //code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code du paramètre : ",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        //old-code
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        $this->add([
            'name' => 'categorie-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé du paramètre : ",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description de la catégorie : ",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'type2',
            ],
        ]);
        //ordre
        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre de la catégorie : ",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'ordre',
            ],
        ]);
        //possibles
        $this->add([
            'type' => Text::class,
            'name' => 'possibles',
            'options' => [
                'label' => "Valeurs possibles (String, Boolean, Number, énumération avec '|' comme séparateur) : ",
            ],
            'attributes' => [
                'id' => 'possibles',
            ],
        ]);
        //Modifiable
        $this->add([
            'type' => Checkbox::class,
            'name' => 'modifiable',
            'options' => [
                'label' => "Est un paramètre modifiable",
            ],
            'attributes' => [
                'id' => 'modifiable',
            ],
        ]);
        //Affichable
        $this->add([
            'type' => Checkbox::class,
            'name' => 'affichable',
            'options' => [
                'label' => "Est un paramètre affichable",
            ],
            'attributes' => [
                'id' => 'affichable',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'next',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);
        //input filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'code'       => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà pour ce paramètre/catégorie",
                        ],
                        'callback' => function ($value, $context = []) {
                            if($context['categorie-code'] . "-" . $value ==  $context['old-code']) return true;
                            return ($this->getParametreService()->getParametreByCode($context['categorie-code'], $value) == null);
                        },
                    ],
                ]],
            ],
            'libelle'    => [     'required' => true, ],
            'possibles'  => [     'required' => false, ],
            'ordre'      => [     'required' => true, ],
            'modifiable' => [     'required' => false, ],
            'affichable' => [     'required' => false, ],
        ]));
    }

    public function setOldCode($value): void
    {
        $this->get('old-code')->setValue($value);
    }

    public function setCategorie(?Categorie $categorie): void
    {
        if ($categorie !== null) {
            $this->get('categorie-code')->setValue($categorie->getCode());
        }
    }
}