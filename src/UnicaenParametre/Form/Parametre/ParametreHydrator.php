<?php

namespace UnicaenParametre\Form\Parametre;

use UnicaenParametre\Entity\Db\Parametre;
use Laminas\Hydrator\HydratorInterface;

class ParametreHydrator implements HydratorInterface {

    public function extract($object) : array
    {
        /** @var Parametre $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'possibles' => $object->getValeursPossibles(),
            'ordre' => $object->getOrdre(),
            'modifiable' => $object->isModifiable(),
            'affichable' => $object->isAffichable(),
        ];
        return $data;
    }

    public function hydrate(array $data, $object) : object
    {
        $code = (isset($data['code']) and trim($data['code']) !== '')?trim($data['code']):null;
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '')?trim($data['libelle']):null;
        $description = (isset($data['description']) and trim($data['description']) !== '')?trim($data['description']):null;
        $valeurs = (isset($data['possibles']) and trim($data['possibles']) !== '')?trim($data['possibles']):null;
        $ordre = (isset($data['ordre']) and trim($data['ordre']) !== '')?trim($data['ordre']):null;
        $modifiable = (isset($data['modifiable'])?$data['modifiable']:false);
        $affichable = (isset($data['affichable'])?$data['affichable']:false);

        /** @var Parametre $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setValeursPossibles($valeurs);
        $object->setModifiable($modifiable);
        $object->setAffichable($affichable);
        $object->setOrdre($ordre);
        return $object;
    }


}