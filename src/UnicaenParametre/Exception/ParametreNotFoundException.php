<?php

namespace UnicaenParametre\Exception;

use RuntimeException;

class ParametreNotFoundException extends RuntimeException {}