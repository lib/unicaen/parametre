<?php

namespace UnicaenParametre\Exception;

use RuntimeException;

class ParametreMalTypeException extends RuntimeException {}