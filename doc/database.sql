-- CREATION DES TABLES --------------------------------------------------------------------------

-- table catégorie
create table unicaen_parametre_categorie
(
    id serial not null,
    code varchar(1024) not null,
    libelle varchar(1024) not null,
    description text,
    ordre integer default 9999
);

create unique index unicaen_parametre_categorie_code_uindex
    on unicaen_parametre_categorie (code);

create unique index unicaen_parametre_categorie_id_uindex
    on unicaen_parametre_categorie (id);

alter table unicaen_parametre_categorie
    add constraint unicaen_parametre_categorie_pk
        primary key (id);

-- table parametre
create table unicaen_parametre_parametre
(
    id serial not null
        constraint unicaen_parametre_parametre_pk
            primary key,
    categorie_id integer not null
        constraint unicaen_parametre_parametre_unicaen_parametre_categorie_id_fk
            references unicaen_parametre_categorie,
    code varchar(1024) not null,
    libelle varchar(1024) not null,
    description text,
    valeurs_possibles text,
    valeur text,
    ordre integer default 9999,
    modifiable boolean not null default true,
    affichable boolean not null default true
);

create unique index unicaen_parametre_parametre_id_uindex
    on unicaen_parametre_parametre (id);

create unique index unicaen_parametre_parametre_code_categorie_id_uindex
    on unicaen_parametre_parametre (code, categorie_id);

-- PRIVILEGES ----------------------------------------------------------------------------------------------------------

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('parametrecategorie', 'UnicaenParametre - Gestion des catégories de paramètres', 70000, 'UnicaenParametre\Provider\Privilege');

INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'parametrecategorie_index', 'Affichage de l''index des paramètres', 10 UNION
    SELECT 'parametrecategorie_afficher', 'Affichage des détails d''une catégorie', 20 UNION
    SELECT 'parametrecategorie_ajouter', 'Ajouter une catégorie de paramètre', 30 UNION
    SELECT 'parametrecategorie_modifier', 'Modifier une catégorie de paramètre', 40 UNION
    SELECT 'parametrecategorie_supprimer', 'Supprimer une catégorie de paramètre', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'parametrecategorie'
;

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('parametre', 'UnicaenParametre - Gestion des paramètres', 70001, 'UnicaenParametre\Provider\Privilege');

INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'parametre_afficher', 'Afficher un paramètre', 10 UNION
    SELECT 'parametre_afficher_masquer', 'Afficher un paramètre masqué', 15 UNION
    SELECT 'parametre_ajouter', 'Ajouter un paramètre', 20 UNION
    SELECT 'parametre_modifier', 'Modifier un paramètre', 30 UNION
    SELECT 'parametre_supprimer', 'Supprimer un paramètre', 50 UNION
    SELECT 'parametre_valeur', 'Modifier la valeur d''un parametre', 100
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'parametre'
;
--TODO penser à accorder les privilèges selon le besoin