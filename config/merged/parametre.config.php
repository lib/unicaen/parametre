<?php

namespace UnicaenParametre;

use UnicaenParametre\Controller\ParametreController;
use UnicaenParametre\Controller\ParametreControllerFactory;
use UnicaenParametre\Form\Parametre\ParametreForm;
use UnicaenParametre\Form\Parametre\ParametreFormFactory;
use UnicaenParametre\Provider\Privilege\ParametrePrivileges;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenParametre\Service\Parametre\ParametreServiceFactory;
use UnicaenParametre\View\Helper\ParametreValueViewHelper;
use UnicaenParametre\View\Helper\ParametreValueViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => ParametreController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        ParametrePrivileges::PARAMETRE_AJOUTER,
                    ]
                ],
                [
                    'controller' => ParametreController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        ParametrePrivileges::PARAMETRE_MODIFIER,
                    ],
                ],
                [
                    'controller' => ParametreController::class,
                    'action' => [
                        'modifier-valeur',
                        'reinitialiser-valeur',
                    ],
                    'privileges' => [
                        ParametrePrivileges::PARAMETRE_VALEUR,
                    ],
                ],
                [
                    'controller' => ParametreController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        ParametrePrivileges::PARAMETRE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'parametre' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/parametre',
                    'defaults' => [
                       'controller' => ParametreController::class,
                        'action' => 'ajouter'
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'ajouter' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/ajouter/:categorie',
                            'defaults' => [
                                /** @see ParametreController::ajouterAction() */
                                'action' => 'ajouter'
                            ],
                        ],
                    ],
                    'modifier' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/modifier/:parametre',
                            'defaults' => [
                                /** @see ParametreController::modifierAction() */
                                'action' => 'modifier'
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/supprimer/:parametre',
                            'defaults' => [
                                /** @see ParametreController::supprimerAction() */
                                'action' => 'supprimer'
                            ],
                        ],
                    ],
                    'modifier-valeur' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/modifier-valeur/:parametre',
                            'defaults' => [
                                /** @see ParametreController::modifierValeurAction() */
                                'action' => 'modifier-valeur'
                            ],
                        ],
                    ],
                    'reinitialiser-valeur' => [
                        'type'  => Segment::class,
                        'options' => [
                            'route'    => '/reinitialiser-valeur/:parametre',
                            'defaults' => [
                                /** @see ParametreController::reinitialiserValeurAction() */
                                'action' => 'reinitialiser-valeur'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            ParametreService::class => ParametreServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            ParametreController::class => ParametreControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            ParametreForm::class => ParametreFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            ParametreService::class => ParametreServiceFactory::class,
        ],
    ],

    'view_helpers' => [
        'factories' => [
            ParametreValueViewHelper::class => ParametreValueViewHelperFactory::class,
        ],
        'aliases' => [
            'parametreValue' => ParametreValueViewHelper::class,
        ],
    ],
];