UnicaenParametre
============

UnicaenParametre est une bibliothèque de gestion de paramètres.
Les paramètres sont désignés par une catégorie et un code.

Le module fournit un menu d'administration permettant la déclaration des paramètres et de leur catégorie, 
ainsi que l'affectation de leur valeur.

Récupération de la valeur d'un paramètre
----------------------------------------

```php
$this->getParametreService()->getValeurForParametre('ma_categorie','mon_parametre');
```

Si le paramètre n'est pas trouvé alors une exception ```ParametreNotFoundException``` est lancée.
Si le paramètre n'est pas typé correctement alors une exception ```ParametreMalTypeException``` est lancée.

***TODO*** À propos du typage :
- Étendre le typage
- Modifier le formulaire pour exploiter les types "déclarés"

Bonnes pratiques
----------------

Afin de mieux référencer les catégories et les paramètres, il est recommandé de mettre en place un provider de parametres déclarant en constante : la catégorie et les paramètres.

```php
<?php

namespace Formation\Provider\Parametre;

class FormationParametres {

    const CATEGORIE = 'FORMATION';

    const NB_PLACE_PRINCIPALE = 'NB_PLACE_PRINCIPALE';
    const NB_PLACE_COMPLEMENTAIRE = 'NB_PLACE_COMPLEMENTAIRE';
}
```

- - - - - - - - - - - - - 


Installation
============

Dans le répertoire **doc** du module, on retrouve le fichier *database.sql* qui permet de construire la table associée au
 module et d'ajouter les privilèges associés.

La navigation est fourni par le fichier ```unicaen-parametre.global.php(.dist)``` et doit être adaptée à votre application.

- - - - - - - - - - - - - 


ChangeLog
==========

**6.1.1**
- Possibilité de marquer les paramètres comme non affichable ce qui masque ceux-ci pour tout ceux qui n'ont pas le privilège "parametre_afficher_masquer"
- Ajout de `htmlentities` pour l'affichage des descriptions de paramètre

**6.1.0**
- Possibilité de marquer les paramètres comme non modifiable ce qui retire l'action "Modifier la valeur" 

**5.0.1**
- Ajout de la fonction de la valeur dans le service ParametreService
- Ajout de l'exception ```ParametreNotFoundException```
- Transformation des classes des entités *à la PHP8*

**5.0.0**
- Finalisation de la migration à BootStrap 5.

Améliorations possibles
-----------------------
- Ajouter des "shorthands" vers l'accés aux valeurs des paramètres.